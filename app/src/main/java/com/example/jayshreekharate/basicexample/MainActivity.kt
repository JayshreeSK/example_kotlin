package com.example.jayshreekharate.basicexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.widget.FrameLayout

class MainActivity : AppCompatActivity() {

    private lateinit var mDrawerll: DrawerLayout
    private lateinit var mNavigationV: NavigationView

    private lateinit var flContainer: FrameLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize();
    }

    private fun initialize() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        mDrawerll = findViewById(R.id.ll_drawer)
        mNavigationV = findViewById(R.id.nav_view)

        flContainer = findViewById(R.id.content_frame)

        mNavigationV.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true

            mDrawerll.closeDrawers()
            true

        }
    }
}
