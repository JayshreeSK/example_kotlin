package com.example.jayshreekharate.basicexample

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class HomeFragment : Fragment()
{


    lateinit var rlContactList : RecyclerView

    lateinit  var activity:MainActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity= context as MainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_home,container,false)
    }
}